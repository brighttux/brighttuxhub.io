---
permalink: /
title: "Hello!"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

I'm Clarence, currently doing my masters as a research scholar in the computer vision field. Prior to this, I was working in the banking industry specializing in service delivery as an application service developer while supporting the applications for 4 years with Hewlett Packard Enterprise. However, my interest lies in computer vision, smart cities, automation, and creation of tools. Lately, I've been facinated with working in the terminal (VIM, Ranger, SSH, etc). Currently residing in Cyberjaya, Selangor, Malaysia.

Current projects:
* Thesis writing
* Retail Video Analytics (side project)
* Learning hiragana (こんいちわ）and Vim


